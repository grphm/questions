<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionsTables extends Migration {

    public function up() {

        Schema::create('solution_questions', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('category_id', FALSE, TRUE)->nullable()->index();
            $table->string('locale', 10)->nullable()->index();
            $table->string('title', 255)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_questions');
    }
}

