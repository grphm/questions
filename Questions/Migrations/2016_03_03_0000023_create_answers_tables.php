<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnswersTables extends Migration {

    public function up() {

        Schema::create('solution_answers', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('question_id', FALSE, TRUE)->index();
            $table->mediumText('content')->nullable();
            $table->integer('correct', FALSE, TRUE)->default(0)->index();
            $table->integer('order', FALSE, TRUE)->default(0)->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_answers');
    }
}

