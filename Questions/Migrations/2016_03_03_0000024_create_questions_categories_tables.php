<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionsCategoriesTables extends Migration {

    public function up() {

        Schema::create('solution_questions_categories', function(Blueprint $table) {

            $table->increments('id');
            $table->string('locale', 10)->nullable()->index();
            $table->string('title', 255)->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_questions_categories');
    }
}