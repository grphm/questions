<?php
namespace STALKER_CMS\Solutions\Questions\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Question extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_questions';
    protected $fillable = ['category_id', 'locale', 'title', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function insert($request) {

        $this->category_id = $request::has('category_id') ? $request::input('category_id') : NULL;
        $this->locale = \App::getLocale();
        $this->title = $request::input('title');
        $this->user_id = \Auth::id();
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->category_id = $request::input('category_id');
        $model->title = $request::input('title');
        $model->user_id = \Auth::id();
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function answers() {

        return $this->hasMany('\STALKER_CMS\Solutions\Questions\Models\Answer', 'question_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}