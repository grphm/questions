<?php
namespace STALKER_CMS\Solutions\Questions\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Answer extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_answers';
    protected $fillable = ['question_id', 'title', 'correct'];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function insert($request) {

        $model = new Answer();
        $model->question_id = $request['question_id'];
        $model->content = $request['content'];
        $model->correct = $request['correct'];
        $model->order = $this::max('order') + 1;
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->content = $request::input('content');
        $model->correct = $request::has('correct') ? TRUE : FALSE;
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['question_id' => 'required', 'title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}