<?php
namespace STALKER_CMS\Solutions\Questions\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Categories extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_questions_categories';
    protected $fillable = ['locale', 'title'];
    protected $hidden = [];
    protected $guarded = [];
    public $timestamps = FALSE;

    public function insert($request) {

        $this->locale = \App::getLocale();
        $this->title = $request::input('title');
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function questions() {

        return $this->hasMany('\STALKER_CMS\Solutions\Questions\Models\Question', 'question_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}