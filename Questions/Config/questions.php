<?php
return [
    'package_name' => 'solutions_questions',
    'package_title' => ['ru' => 'Вопросы', 'en' => 'Questions', 'es' => 'Preguntas'],
    'package_icon' => 'zmdi zmdi-comments',
    'relations' => [],
    'package_description' => [
        'ru' => 'Позволяет создавать перечень вопросов',
        'en' => 'It allows you to create a list of questions',
        'es' => 'Se le permite crear una lista de preguntas'
    ],
    'version' => [
        'ver' => 1.0,
        'date' => '03.03.2016'
    ],
    'categories' => [
    ]
];