<?php
return [
    'package' => 'solutions_questions',
    'title' => ['ru' => 'Вопросы', 'en' => 'Questions', 'es' => 'Preguntas'],
    'route' => 'solutions.questions.index',
    'icon' => 'zmdi zmdi-pin-help',
    'menu_child' => []
];