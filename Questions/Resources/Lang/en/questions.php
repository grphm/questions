<?php
return [
    'answers' => 'Answers',
    'edit' => 'Edit',
    'empty' => 'List is empty',
    'delete' => [
        'question' => 'Delete question',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding question',
        'form' => [
            'add_answer' => 'Add answer',
            'remove-element' => 'Delete answer',
            'answer' => 'Answer',
            'category' => 'Category',
            'title' => 'Question',
            'content' => 'Content',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit question',
        'form' => [
            'add_answer' => 'Add answer',
            'remove-element' => 'Delete answer',
            'answer' => 'Answer',
            'category' => 'Category',
            'title' => 'Question',
            'submit' => 'Save'
        ]
    ]
];