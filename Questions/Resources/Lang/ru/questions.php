<?php
return [
    'answers' => 'Ответов',
    'edit' => 'Редактировать',
    'empty' => 'Список пустой',
    'delete' => [
        'question' => 'Удалить вопрос',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление вопроса',
        'form' => [
            'add_answer' => 'Добавить ответ',
            'remove-element' => 'Удалить ответ',
            'answer' => 'Ответ',
            'category' => 'Категория',
            'title' => 'Текст вопроса',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование вопроса',
        'form' => [
            'add_answer' => 'Добавить ответ',
            'remove-element' => 'Удалить ответ',
            'answer' => 'Ответ',
            'category' => 'Категория',
            'title' => 'Текст вопроса',
            'submit' => 'Сохранить'
        ]
    ]
];