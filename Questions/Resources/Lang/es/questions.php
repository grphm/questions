<?php
return [
    'answers' => 'Respuestas',
    'edit' => 'Editar',
    'embed' => 'Código de inserción',
    'delete' => [
        'question' => 'Eliminar pregunta',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Añadir una pregunta',
        'form' => [
            'add_answer' => 'Añadir pregunta',
            'remove-element' => 'Eliminar respuesta',
            'answer' => 'Respuesta',
            'category' => 'Categoría',
            'title' => 'Pregunta',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar pregunta',
        'form' => [
            'add_answer' => 'Añadir pregunta',
            'remove-element' => 'Eliminar respuesta',
            'answer' => 'Respuesta',
            'category' => 'Categoría',
            'title' => 'Pregunta',
            'submit' => 'Guardar'
        ]
    ]
];