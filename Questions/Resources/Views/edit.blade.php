@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('solutions.questions.index') }}">
                <i class="{{ config('solutions_questions::menu.icon') }}"></i> {!! array_translate(config('solutions_questions::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('solutions_questions_lang::questions.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('solutions_questions_lang::questions.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($question, ['route' => ['solutions.questions.update', $question->id], 'class' => 'form-validate', 'id' => 'edit-solutions-question-form', 'method' => 'PUT']) !!}
                <div class="col-sm-9">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('solutions_questions_lang::questions.insert.form.title')</label>
                    </div>
                    <div id="form-structure">
                        @foreach($answers as $answer)
                            @include('solutions_questions_views::assets.answer', ['answer_title' => $answer->content, 'answer_correct' => $answer->correct])
                        @endforeach
                    </div>
                    <button type="button" id="addFormElement" class="btn bgm-teal waves-effect pull-right m-t-10">
                        <i class="zmdi zmdi-plus"></i> @lang('solutions_questions_lang::questions.insert.form.add_answer')
                    </button>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('solutions_questions_lang::questions.insert.form.category')</p>
                        {!! Form::select('category_id', $categories, NULL, ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_questions_lang::questions.insert.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="hidden">
        @include('solutions_questions_views::assets.answer')
    </div>
@stop
@section('scripts_before')
    <script>
        $(function () {
            var number_element = 0;
            $("#form-structure .clone-element").each(function (index_clone_element, clone_element) {
                $(clone_element).removeClass('answer-template');
                number_element++;
                $(clone_element).find(".element-input-name").each(function (index, element) {
                    var name = $(element).attr('name');
                    $(element).attr('name', 'answer[' + number_element + '][' + name + ']');
                });
            });

            $("#addFormElement").click(function () {
                var clone_elements_count = $('#form-structure .clone-element').length;
                if (clone_elements_count > 0) {
                    $(".answer-template").clone(true).insertAfter($('#form-structure .clone-element:last'));
                } else {
                    $(".answer-template").clone(true).prependTo('#form-structure');
                }
                var clone_element = $("#form-structure .clone-element:last");
                $(clone_element).removeAttr('class').addClass('clone-element');
                $(clone_element).find(".element-input-name").each(function (index, element) {
                    var name = $(element).attr('name');
                    var next_element = clone_elements_count + 1;
                    $(element).attr('name', 'answer[' + next_element + '][' + name + ']');
                });
            });
            $(document).on('click', '.remove-clone-element', function (event) {
                event.preventDefault();
                $(this).parents('.clone-element').remove();
            })
        });
    </script>
@stop
@section('scripts_after')
    <script>
        var form = $("#edit-solutions-question-form");
        BASIC.currentForm = form;
        BASIC.validateOptions.rules = {title: {required: true}};
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);

        BASIC.cropping.ratio = false;
        BASIC.cropping.boxResizable = true;
        BASIC.cropping.movable = true;
        BASIC.cropping.init(element);
    </script>
@stop