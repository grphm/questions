@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_questions::menu.icon') }}"></i> {!! array_translate(config('solutions_questions::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_questions::menu.icon') }}"></i> {!! array_translate(config('solutions_questions::menu.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('solutions_questions', 'create', FALSE))
        @BtnAdd('solutions.questions.create')
    @endif
    <div class="card">
        <div class="card-body card-padding m-h-250">
            <div class="card-body card-padding m-h-250">
                @if($questions->count())
                    <div class="listview lv-bordered lv-lg">
                        <div class="lv-body">
                            @foreach($questions as $question)
                                <div class="js-item-container lv-item media">
                                    <div class="media-body">
                                        <div class="lv-title">{{ $question->title }}</div>
                                        <ul class="lv-attrs">
                                            <li>ID: @numDimensions($question->id)</li>
                                            <li>
                                                @lang('solutions_questions_lang::questions.answers'):
                                                {{ count($question->answers) }}
                                            </li>
                                        </ul>
                                        <div class="lv-actions actions dropdown">
                                            <a aria-expanded="true" data-toggle="dropdown" href="">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{ route('solutions.questions.edit', $question->id) }}">
                                                        @lang('solutions_questions_lang::questions.edit')
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    {!! Form::open(['route' => ['solutions.questions.destroy', $question->id], 'method' => 'DELETE']) !!}
                                                    <button type="submit"
                                                            class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                            autocomplete="off"
                                                            data-question="@lang('solutions_questions_lang::questions.delete.question') &laquo;{{ $question->title }}&raquo;?"
                                                            data-confirmbuttontext="@lang('solutions_questions_lang::questions.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('solutions_questions_lang::questions.delete.cancelbuttontext')">
                                                        @lang('solutions_questions_lang::questions.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @else
                    <h2 class="f-16 c-gray">@lang('solutions_questions_lang::questions.empty')</h2>
                @endif
            </div>
        </div>
    </div>
@stop