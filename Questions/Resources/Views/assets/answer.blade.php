<div class="answer-template clone-element">
    <div class="f-500 c-gray m-b-25">
        <ul class="actions pull-right">
            <li class="dropdown">
                <a aria-expanded="false" data-toggle="dropdown" href="">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a class="remove-clone-element"
                           href="javascript:void(0)">@lang('solutions_questions_lang::questions.insert.form.remove-element')
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
    {!! Form::hidden('correct', isset($answer_correct) ? $answer_correct : NULL, ['class' => 'element-input-name']) !!}
    <div class="form-group fg-float">
        <div class="fg-line">
            {!! Form::textarea('title', isset($answer_title) ? $answer_title : NULL, ['class' => 'form-control fg-input element-input-name', 'rows' => 2]) !!}
        </div>
        <label class="fg-label">@lang('solutions_questions_lang::questions.insert.form.answer')</label>
    </div>
</div>