<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function() {

    \Route::resource('questions', 'QuestionsController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.questions.index',
                'create' => 'solutions.questions.create',
                'store' => 'solutions.questions.store',
                'edit' => 'solutions.questions.edit',
                'update' => 'solutions.questions.update',
                'destroy' => 'solutions.questions.destroy'
            ]
        ]
    );
});