<?php
namespace STALKER_CMS\Solutions\Questions\Http\Controllers;

use STALKER_CMS\Solutions\Questions\Models\Answer;
use STALKER_CMS\Solutions\Questions\Models\Categories;
use STALKER_CMS\Solutions\Questions\Models\Question;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class QuestionsController extends ModuleController implements CrudInterface {

    protected $question;
    protected $answer;

    public function __construct(Question $question, Answer $answer) {

        $this->question = $question;
        $this->answer = $answer;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('solutions_questions', 'questions');
        $questions = $this->question->orderBy('created_at', 'DESC')->with('answers')->get();
        return view('solutions_questions_views::index', compact('questions'));
    }

    public function create() {

        \PermissionsController::allowPermission('solutions_questions', 'create');
        return view('solutions_questions_views::create', ['categories' => Categories::lists('title', 'id')]);
    }

    public function store() {

        \PermissionsController::allowPermission('solutions_questions', 'create');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->question->getStoreRules())):
            $question = $this->question->insert($request);
            if($request::has('answer')):
                foreach($request::input('answer') as $answer):
                    $this->answer->insert(['question_id' => $question->id, 'content' => $answer['title'], 'correct' => $answer['correct']]);
                endforeach;
            endif;
            return \ResponseController::success(201)->redirect(route('solutions.questions.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {

        \PermissionsController::allowPermission('solutions_questions', 'edit');
        return view('solutions_questions_views::edit', [
            'categories' => Categories::lists('title', 'id'),
            'question' => $this->question->findOrFail($id),
            'answers' => $this->answer->whereQuestionId($id)->get()
        ]);
    }

    public function update($id) {

        \PermissionsController::allowPermission('solutions_questions', 'edit');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->question->getUpdateRules())):
            $question = $this->question->findOrFail($id);
            $this->question->replace($id, $request);
            $this->answer->whereQuestionId($id)->delete();
            if($request::has('answer')):
                foreach($request::input('answer') as $answer):
                    $this->answer->insert(['question_id' => $question->id, 'content' => $answer['title'], 'correct' => $answer['correct']]);
                endforeach;
            endif;
            return \ResponseController::success(202)->redirect(route('solutions.questions.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($id) {

        \PermissionsController::allowPermission('solutions_questions', 'delete');
        \RequestController::isAJAX()->init();
        $this->question->findOrFail($id)->delete();
        $this->answer->whereQuestionId($id)->delete();
        return \ResponseController::success(1203)->redirect(route('solutions.questions.index'))->json();
    }
}