<?php
namespace STALKER_CMS\Solutions\Questions\Providers;

use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('solutions_questions_views');
        $this->registerLocalization('solutions_questions_lang');
        $this->registerConfig('solutions_questions::config', 'Config/questions.php');
        $this->registerSettings('solutions_questions::settings', 'Config/settings.php');
        $this->registerActions('solutions_questions::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_questions::menu', 'Config/menu.php');
    }

    public function register() {
    }
}
